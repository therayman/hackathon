class CreateSalesMen < ActiveRecord::Migration
  def change
    create_table :sales_men do |t|
      t.string :name

      t.timestamps
    end
  end
end
