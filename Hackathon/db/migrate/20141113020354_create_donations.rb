class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.integer :donation
      t.date :date
      t.integer :donorID

      t.timestamps
    end
  end
end
