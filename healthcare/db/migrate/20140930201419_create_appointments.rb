class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :specialist_id
      t.integer :patient_id
      t.text :complaint
      t.string :appointment_date
      t.string :date

      t.timestamps
    end
  end
end
