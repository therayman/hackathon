class AsswipesController < ApplicationController
  before_action :set_asswipe, only: [:show, :edit, :update, :destroy]

  # GET /asswipes
  # GET /asswipes.json
  def index
    @asswipes = Asswipe.all
  end

  # GET /asswipes/1
  # GET /asswipes/1.json
  def show
  end

  # GET /asswipes/new
  def new
    @asswipe = Asswipe.new
  end

  # GET /asswipes/1/edit
  def edit
  end

  # POST /asswipes
  # POST /asswipes.json
  def create
    @asswipe = Asswipe.new(asswipe_params)

    respond_to do |format|
      if @asswipe.save
        format.html { redirect_to @asswipe, notice: 'Asswipe was successfully created.' }
        format.json { render :show, status: :created, location: @asswipe }
      else
        format.html { render :new }
        format.json { render json: @asswipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /asswipes/1
  # PATCH/PUT /asswipes/1.json
  def update
    respond_to do |format|
      if @asswipe.update(asswipe_params)
        format.html { redirect_to @asswipe, notice: 'Asswipe was successfully updated.' }
        format.json { render :show, status: :ok, location: @asswipe }
      else
        format.html { render :edit }
        format.json { render json: @asswipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /asswipes/1
  # DELETE /asswipes/1.json
  def destroy
    @asswipe.destroy
    respond_to do |format|
      format.html { redirect_to asswipes_url, notice: 'Asswipe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_asswipe
      @asswipe = Asswipe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def asswipe_params
      params.require(:asswipe).permit(:name)
    end
end
