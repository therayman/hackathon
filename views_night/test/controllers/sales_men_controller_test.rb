require 'test_helper'

class SalesMenControllerTest < ActionController::TestCase
  setup do
    @sales_man = sales_men(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sales_men)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sales_man" do
    assert_difference('SalesMan.count') do
      post :create, sales_man: { name: @sales_man.name }
    end

    assert_redirected_to sales_man_path(assigns(:sales_man))
  end

  test "should show sales_man" do
    get :show, id: @sales_man
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sales_man
    assert_response :success
  end

  test "should update sales_man" do
    patch :update, id: @sales_man, sales_man: { name: @sales_man.name }
    assert_redirected_to sales_man_path(assigns(:sales_man))
  end

  test "should destroy sales_man" do
    assert_difference('SalesMan.count', -1) do
      delete :destroy, id: @sales_man
    end

    assert_redirected_to sales_men_path
  end
end
