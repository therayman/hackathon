json.array!(@sales_men) do |sales_man|
  json.extract! sales_man, :id, :name
  json.url sales_man_url(sales_man, format: :json)
end
