class CreateAsswipes < ActiveRecord::Migration
  def change
    create_table :asswipes do |t|
      t.string :name

      t.timestamps
    end
  end
end
