json.array!(@donations) do |donation|
  json.extract! donation, :id, :donation, :date, :donorID
  json.url donation_url(donation, format: :json)
end
