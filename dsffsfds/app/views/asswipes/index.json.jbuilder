json.array!(@asswipes) do |asswipe|
  json.extract! asswipe, :id, :name
  json.url asswipe_url(asswipe, format: :json)
end
