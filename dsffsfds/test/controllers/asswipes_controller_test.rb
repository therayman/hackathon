require 'test_helper'

class AsswipesControllerTest < ActionController::TestCase
  setup do
    @asswipe = asswipes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:asswipes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create asswipe" do
    assert_difference('Asswipe.count') do
      post :create, asswipe: { name: @asswipe.name }
    end

    assert_redirected_to asswipe_path(assigns(:asswipe))
  end

  test "should show asswipe" do
    get :show, id: @asswipe
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @asswipe
    assert_response :success
  end

  test "should update asswipe" do
    patch :update, id: @asswipe, asswipe: { name: @asswipe.name }
    assert_redirected_to asswipe_path(assigns(:asswipe))
  end

  test "should destroy asswipe" do
    assert_difference('Asswipe.count', -1) do
      delete :destroy, id: @asswipe
    end

    assert_redirected_to asswipes_path
  end
end
